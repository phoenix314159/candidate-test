import types from '../actions/types'

export default (state = {}, action) => {
  const {type} = action
  switch (type) {
    case types.HIDE_BUTTON:
      let {payload: {hide, value, inputArr}} = action
      if(inputArr) {
        inputArr = inputArr.concat(value).slice(-1)
      }
      return hide ? {...state, hide, value: ''} : {...state, hide, value, inputArr}
    default:
      return state
  }
}
